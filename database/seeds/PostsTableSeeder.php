<?php

use Illuminate\Database\Seeder;
use App\Post;
use App\Category;
use App\Tag;
use Illuminate\Support\Facades\Hash;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $author1 = App\User::create([
            'name' => 'Prativa',
            'email' => 'pparajuli980@gmail.com',
            'password' => Hash::make('12345678q')
            
        ]);
        $author2 = App\User::create([
            'name' => 'Pp',
            'email' => 'parajuli980@gmail.com',
            'password' => Hash::make('12345678q')
            
        ]);

        $category1 = Category::create([
            'name' => 'News',
        ]);
        $category2 = Category::create([
            'name' => 'Design and Print',

        ]);
        $category3 = Category::create([
            'name' => 'Programs',
        ]);


        $post1 = $author1->posts()->create([
            'title' => 'We are the Kayakalpa Families',
            'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. ',
            'content'   => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose.',
            'category_id' => $category1->id,
            'image' => 'posts/6.jpg'
        ]);

        $post2 = $author2->posts()->create([
            'title' => 'Nepal Visit 2020',
            'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. ',
            'content'   => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose.',
            'category_id' => $category2->id,
            'image' => 'posts/9.jpg'
        ]);

        $post3 =$author1->posts()->create([
            'title' => 'Kayakalpa Families',
            'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. ',
            'content'   => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose.',
            'category_id' => $category3->id,
            'image' => 'posts/11.jpg'
        ]);

        $post4 = $author2->posts()->create([
            'title' => ' Families',
            'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. ',
            'content'   => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose.',
            'category_id' => $category1->id,
            'image' => 'posts/4.jpg'

        ]);


        $tag1 = Tag::create([
            'name' => 'job',
        ]);
        $tag2 = Tag::create([
            'name' => 'create',

        ]);
        $tag3 = Tag::create([
            'name' => 'records',
        ]);

        $post1->tags()->attach([$tag1->id, $tag2->id]);

        $post2->tags()->attach([$tag2->id, $tag3->id]);

        $post3->tags()->attach([$tag1->id, $tag3->id]);
        
        $post4->tags()->attach([$tag3->id, $tag2->id]);
    }
}
