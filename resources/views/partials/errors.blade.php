@if ($errors->any())
     <div class="alert alert-danger">
       <ul class="list-group">
         @foreach ($errors->all() as $e)
         <li class="list-group-item text-danger">
           {{ $e }}
         </li>

             
         @endforeach
       </ul>
     </div>
        
    @endif